import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { User } from '../../interfaces/user.interface';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { Facebook } from '@ionic-native/facebook';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  user: User = {};

  constructor( private afAuth: AngularFireAuth, private platform: Platform,
               private storage: Storage, private fireStore: AngularFirestore,
              private fb: Facebook) {
    this.cargarStorage();
  }

  public signInWithFacebook() {

    let promise = new Promise((resolve, reject) => {
      if(this.platform.is('cordova')){
        this.fb.login(['email', 'public_profile']).then(res => {
          const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
          return firebase.auth().signInWithCredential(facebookCredential).then((res: any) => {
            this.user.name = res.user.displayName;
            this.user.email = res.user.email;

            this.saveUserFirebase(this.user).then((res) => {
              this.guardarStorage();
              resolve(true);
            });
          });
        })
      }else{

        this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((res:any) => {
        console.log(res);
        this.user.name = res.user.displayName;
        this.user.email = res.user.email;

        this.saveUserFirebase(this.user).then((res) => {
          this.guardarStorage();
          resolve(true);
        });
      })
      .catch(err => {
        console.log(err);
        resolve(false);
      }
      );
      }
    });

    return promise;
  }

  signOut() {
    this.afAuth.auth.signOut();
  }

  private guardarStorage(){
    if (this.platform.is('cordova')) {
      //dispositivo
      this.storage.set( 'name', this.user.name );
      this.storage.set( 'email', this.user.email );
      this.storage.set( 'id', this.user.id );
    }else{
      // computadora
      
      localStorage.setItem('name', this.user.name);
      localStorage.setItem('email', this.user.email);
      localStorage.setItem('id', this.user.id);
    }
  }

  cargarStorage(){

    let promesa = new Promise( (resolve, reject) => {
      if (this.platform.is('cordova')) {
        // Dispositivo
        this.storage.ready()
          .then( () => {
            this.storage.get('email')
              .then( (email: any) => {
                if (email) {
                  this.user.email = email;
                }
              });

              this.storage.get('id')
              .then( (id: any) => {
                if (id) {
                  this.user.id = id;
                }
              });

              this.storage.get('name')
                .then( (name: any) => {
                  if (name) {
                    this.user.name = name;
                  }
                  resolve();
                });

          });

      }else{
        //Computadora
        if (localStorage.getItem('email')) {
        //Existe item en el local storage
            this.user.name = localStorage.getItem('name');
            this.user.email = localStorage.getItem('email');
            //console.log('Id Usuario', this.id_usuario);

        }
        resolve();
      }
    });

    return promesa;

  }

  saveUserFirebase( user: User ){

    let promesa = new Promise( (resolve, reject)=>{

      this.verifyDuplicates(user.email).then((res) => {
        if(res){
          resolve(true);
        }else{

          let userCollection = this.fireStore.collection<any>('users');

          userCollection.add({
            name: user.name,
            email: user.email
          }).then((res) => {
            this.user.id = res.id;
            resolve(true);
          });
        }
      });
    });

    return promesa;

  }

  verifyDuplicates(email: string){
    let promise = new Promise((resolve, reject) => {

      let result = this.fireStore.collection('users', ref => 
        ref.where('email', '==', email)).snapshotChanges();

      result.subscribe(data => {

        if(data.length){
          data.map(e => {
            this.user.id = e.payload.doc.id;
            resolve(true);
          })
        }else{
          resolve(false);
        }
      });

    });

    return promise;
  }

}
