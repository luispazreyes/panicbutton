import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Alert } from '../../interfaces/alert.interface';
/*
  Generated class for the AlertProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlertProvider {

  constructor( private fireStore: AngularFirestore )  {
    console.log('Hello AlertProvider Provider');
  }

  sendAlert(alertDetails: Alert){
    let promise = new Promise((resolve, reject) => {
      this.fireStore.collection<any>('alerts').add(alertDetails).then((res) => {
        console.log(res);
        resolve(true);
      });
    });

    return promise;
  }

}
