export interface Alert{
    email: string;
    lat: number;
    lng: number;
    time: string;
}