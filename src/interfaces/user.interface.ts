export interface User {
    id?: string;
    email?: string;
    name?: string;
    sangre?: string;
    edad?: number;
}