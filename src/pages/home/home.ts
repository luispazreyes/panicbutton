import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { Alert } from '../../interfaces/alert.interface';
import { UserProvider } from '../../providers/user/user';
import { AlertProvider } from '../../providers/alert/alert';

import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  alertDetails: Alert;
  lat: number;
  lng: number;

  constructor(public navCtrl: NavController, public _up: UserProvider,
              public _ap: AlertProvider, private geo: Geolocation,
              private loadingCtrl: LoadingController, private alertCtrl: AlertController) {

  }

  configureAlert(){

    let loading = this.loadingCtrl.create({
      content: 'Enviando Alerta, Mantenga la calma'
    });

    loading.present();
    
    this.getPosition().then((res) => {
      this.alertDetails = {
        email: this._up.user.email,
        lat: this.lat,
        lng: this.lng,
        time: new Date().toString()
      }

      this._ap.sendAlert(this.alertDetails).then((res) => {
        if(res){
          loading.dismiss();
          this.alertCtrl.create({
            title: 'Éxito',
            subTitle: 'Su alerta ha sido enviada exitosamente',
            buttons: ['OK']
          }).present();
        }
      });
      
    });

  }

  getPosition(){

    let promise = new Promise((resolve, reject) => {
      this.geo.getCurrentPosition().then((res) => {
        this.lat = res.coords.latitude;
        this.lng = res.coords.longitude;

        resolve(true);
      });
    });

    return promise;
    
  }

}
