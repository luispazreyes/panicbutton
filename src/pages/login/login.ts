import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public _up: UserProvider, private loadingCtrl: LoadingController) {
  }

  login(){

    let loading = this.loadingCtrl.create({
      content: 'Verificando Credenciales'
    });

    loading.present();

    this._up.signInWithFacebook()
      .then((res) => {
        if(res){
          
          loading.dismiss();
          this.navCtrl.setRoot(HomePage);
        }
      });
  }

}
