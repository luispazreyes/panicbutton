import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';


import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { UserProvider } from '../providers/user/user';

import { IonicStorageModule } from '@ionic/storage';
import { AlertProvider } from '../providers/alert/alert';

//Native plugins
import { Geolocation } from '@ionic-native/geolocation';
import { Facebook } from '@ionic-native/facebook';


export const firebaseConfig = {
  apiKey: "AIzaSyBBFCd1DTnTJXr07H73fWup59hSWpchQog",
  authDomain: "panicbutton-22409.firebaseapp.com",
  databaseURL: "https://panicbutton-22409.firebaseio.com",
  projectId: "panicbutton-22409",
  storageBucket: "panicbutton-22409.appspot.com",
  messagingSenderId: "574360834801",
  appId: "1:574360834801:web:24c1af58a78af2ded6cf50",
  measurementId: "G-MCBC3TVJKQ"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserProvider,
    AlertProvider,
    Geolocation,
    Facebook
  ]
})
export class AppModule {}
